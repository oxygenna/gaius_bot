import React, { Component } from 'react';
import Header from './Header';
import MessageForm from './MessageForm';
import MessageList from './MessageList';
import Footer from './Footer';
import TwilioChat from 'twilio-chat';
import Button from '@material-ui/core/Button';
import Modal from './Modal';
import './theme.css';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      username: null,
      channel: null,
      botSid: null,
      channelSid: null,
      typingIndicator: false,
      chatDisable: true,
      haslink: false,
      language: 'DE',
      anchorEl: null,
    };
    this.componentCleanup = this.componentCleanup.bind(this);
  }

  componentDidMount = () => {
    window.addEventListener('beforeunload', this.componentCleanup);
  };

  componentCleanup() {
    this.deleteChannel();
  }

  componentWillUnmount() {
    this.componentCleanup();
    window.removeEventListener('beforeunload', this.componentCleanup);
  }

  getToken = () => {
    return new Promise(async (resolve, reject) => {
      const tokenID = Math.random().toString(36).substring(7);
      await fetch('https://europe-west1-gaius-chatbot-serverless.cloudfunctions.net/g-serverless-dev-createtoken', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ tokenID }),
      })
        .then((res) => res.json())
        .then((result) => {
          this.setState({ username: tokenID });
          resolve(result);
        });
    });
  };

  deleteChannel = () => {
    const channelSid = this.state.channelSid;
    fetch('https://europe-west1-gaius-chatbot-serverless.cloudfunctions.net/g-serverless-dev-deletechannel', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ channelSid }),
    });
  };

  addBot = async (channelSid) => {
    const botSid = await fetch('https://europe-west1-gaius-chatbot-serverless.cloudfunctions.net/g-serverless-dev-addbot', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ channelSid }),
    });
    this.setState({ botSid });
  };

  initChatBot = () => {
    this.getToken()
      .then(this.createChatClient)
      .then(this.createChannel)
      .catch((error) => {
        this.addMessage({ body: `Error: ${error.message}` });
      });
  };

  createChatClient = (token) => {
    return new Promise((resolve, reject) => {
      resolve(new TwilioChat(token.jwt));
    });
  };

  joinChannel = (chatClient, uniqueName) => {
    chatClient.getSubscribedChannels().then(() => {
      chatClient.getChannelByUniqueName(uniqueName).then((channel) => {
        this.setState({ channel });
        channel.join().then(() => {
          this.setState({ chatDisable: false });
          window.addEventListener('beforeunload', () => channel.leave());
          this.state.channel.sendMessage('Init');
        });
        this.configureChannelEvents(channel);
      });
    });
  };

  createChannel = (chatClient) => {
    return new Promise((resolve, reject) => {
      const uniqueName = 'Channel ' + Math.floor(Math.random() * 1000000000);
      chatClient
        .createChannel({ uniqueName, friendlyName: 'Gaius Bot Chat' })
        .then(() => {
          let channelSid = chatClient.channels.channels;
          channelSid = Array.from(channelSid);
          channelSid = channelSid[0][0];
          this.setState({ channelSid });
          this.addBot(channelSid);
          this.joinChannel(chatClient, uniqueName);
          resolve(chatClient);
        })
        .catch(() => reject(Error('Could not create' + uniqueName + 'channel.')));
    });
  };

  addMessage = (message) => {
    const messageData = { ...message, me: message.author === this.state.username };
    this.setState({
      messages: [...this.state.messages, messageData],
    });
  };

  handleNewMessage = (text) => {
    if (this.state.channel) {
      this.state.channel.sendMessage(text);
    }
  };

  disableChat = () => {
    this.setState({ chatDisable: true });
    document.getElementsByClassName('input-container')[0].style.backgroundColor = '#dfdfdf';
    document.getElementById('textFieldId').style.backgroundColor = '#dfdfdf';
    document.getElementsByClassName('MessageForm')[0].classList.remove('test');
  };

  EnableChat = () => {
    this.setState({ chatDisable: false });
    document.getElementsByClassName('input-container')[0].style.backgroundColor = '#fff';
    document.getElementById('textFieldId').style.backgroundColor = '#fff';
    if (document.getElementsByClassName('MessageForm')[0].classList.contains('test') !== true) {
      document.getElementsByClassName('MessageForm')[0].className += ' test';
    }
  };

  makelink = () => {
    let a = document.createElement('a');
    let link = document.getElementById('linkme');
    a.appendChild(link);
    a.href = 'https://app.gaius.legal';
    let lengthOfClass = document.getElementsByClassName('possible-answers').length - 1;
    document.getElementsByClassName('possible-answers')[lengthOfClass].appendChild(a);
    document.getElementById('linkme').style.order = '2';
  };

  configureChannelEvents = (channel) => {
    /* Author in token is fixed to Me, this part is used for the icons that are going to show up in the chat for the bot & user */
    channel.on('messageAdded', ({ author, body, attributes }) => {
      author !== this.state.username
        ? (author = (
            <div className='bot-icon-section'>
              <img className='bot-icon' src='gaiusbot.svg' alt='prof' />{' '}
            </div>
          ))
        : (author = ''); //Author is "" because we don't want an icon for the user.//

      //fake init message gets vanished, this happens in order to init the bot start the conversation.//
      const bodymsg = (
        <div className={body === 'Init' ? 'toVanish' : 'text-container'}>
          {attributes.translation_de && this.state.language === 'DE' ? attributes.translation_de : body}
        </div>
      );

      if (author !== '') {
        this.setState({ typingIndicator: true });
        const typingDuration = body.length * 10;
        if (attributes.botIsTyping) {
          this.disableChat();
        }

        setTimeout(() => {
          if (typeof attributes.expectedAnswers !== 'undefined') {
            // Bot Section for the posting functionality //
            if (attributes.endflow) {
              this.addMessage({ author, body: bodymsg });
              this.disableChat();
            } else {
              const buttonsToRender = this.possibleAnswers(attributes);
              const bodymsg = (
                <div className='text-container'>
                  {this.state.language === 'DE' ? attributes.translation_de : body}
                  <div className='possible-answers'>{buttonsToRender}</div>
                </div>
              );
              this.addMessage({ author, body: bodymsg });
              this.disableChat();
            }
            //Checking if a button has a link//
            if (this.state.haslink) {
              this.makelink();
            }
          } else {
            if (attributes.endflow) {
              this.addMessage({ author, body: bodymsg });
              this.disableChat();
            } else {
              if (attributes.botIsTyping) {
                this.addMessage({ author, body: bodymsg });
                this.disableChat();
              } else {
                this.addMessage({ author, body: bodymsg });
                this.EnableChat();
              }
            }
          }
          this.setState({ typingIndicator: false });
        }, typingDuration);
      } else {
        //post single message
        this.addMessage({ author, body: bodymsg });
      }
    });
  };

  possibleAnswers = (answers) => {
    const expectedAnswers = this.state.language === 'DE' ? answers.expectedAnswers_de : answers.expectedAnswers;
    let tempClass = ' possible-answers-' + this.state.messages.length;
    if (expectedAnswers.length > 1) {
      if (answers.link) {
        // If Expected answers in buttons have a link. Add id "link me" in order to apply <a> tag using javascript to the button //
        this.setState({ haslink: true });
        const buttonAnswers = expectedAnswers.map((answer, index) => {
          return (
            <div key={index} id='linkme' className={'possible-answers-' + this.state.messages.length}>
              <Button variant='contained' onClick={this.sendMsg.bind(this, { answer }, tempClass)} value={answer}>
                {answer}
              </Button>
            </div>
          );
        });
        return buttonAnswers;
      } else {
        // else just add the possible answer buttons
        this.setState({ haslink: false });
        const buttonAnswers = expectedAnswers.map((answer, index) => {
          return (
            <div key={index} className={'possible-answers-' + this.state.messages.length}>
              <Button variant='contained' onClick={this.sendMsg.bind(this, { answer }, tempClass)} value={answer}>
                {answer}
              </Button>
            </div>
          );
        });
        return buttonAnswers;
      }
    } else {
      // One possible answer case
      this.setState({ haslink: false });
      const answer = expectedAnswers[0];
      const buttonAnswers = (
        <div className={'possible-answers-' + this.state.messages.length}>
          <Button key={1} variant='contained' onClick={this.sendMsg.bind(this, { answer }, tempClass)} value={answer}>
            {answer}
          </Button>
        </div>
      );
      return buttonAnswers;
    }
  };

  sendMsg = (value, tempClass) => {
    this.handleNewMessage(value.answer);
    const items = document.getElementsByClassName(tempClass);
    for (var i = 0; i < items.length; i++) {
      items[i].style.visibility = 'hidden';
      items[i].style.opacity = '0';
      items[i].style.transition = 'visibility 0s linear 400ms, opacity 400ms';
    }
  };

  handleCloseSelectionlanguage = (languageSelected) => {
    this.setState({ anchorEl: null, language: languageSelected });
  };

  handleClickSelectionlanguage = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  render() {
    return (
      <div className='App'>
        <Modal
          language={this.state.language}
          onLanguageSelection={this.handleCloseSelectionlanguage}
          handleClickSelectionlanguage={this.handleClickSelectionlanguage}
          anchorEl={this.state.anchorEl}
          initChatBot={this.initChatBot}
        />
        <Header language={this.state.language} />
        <MessageList messages={this.state.messages} />
        <MessageForm
          typingIndicator={this.state.typingIndicator}
          onMessageSend={this.handleNewMessage}
          chatDisable={this.state.chatDisable}
        />
        <Footer
          language={this.state.language}
          onLanguageSelection={this.handleCloseSelectionlanguage}
          handleClickSelectionlanguage={this.handleClickSelectionlanguage}
          anchorEl={this.state.anchorEl}
        />
      </div>
    );
  }
}

export default App;
