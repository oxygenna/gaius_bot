import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import './Footer.css';
class Footer extends React.Component {
  render() {
    const actionClasses = this.props.classes;
    return (
      <BottomNavigation value={1} showLabels={true} className='Footer'>
        <BottomNavigationAction
          className='footer-item1'
          classes={actionClasses}
          label={this.props.language === 'DE' ? 'Lotse' : 'Guide '}
          icon={<img src='./search.svg' alt='Search'></img>}
        />

        <BottomNavigationAction
          className='footer-item2'
          classes={actionClasses}
          label='Gaius'
          icon={
            <a href='https:app.gaius.legal' target='_blank' rel='noopener noreferrer' alt='gaius'>
              <img src='./chat.svg' alt='Chat'></img>
            </a>
          }
        />

        <BottomNavigationAction
          className='footer-item3'
          classes={actionClasses}
          label={this.props.language === 'DE' ? 'Mehr' : 'More'}
          icon={<img src='./burger.svg' alt='More'></img>}
        />
      </BottomNavigation>
    );
  }
}
export default Footer;
