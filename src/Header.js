import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import './Header.css';
export default function Header(props) {
  const [profileMenu, setProfileMenu] = React.useState(null);

  const handleClick = (event) => {
    setProfileMenu(event.currentTarget);
  };

  const handleClose = () => {
    setProfileMenu(null);
  };

  return (
    <div className='header'>
      <div className='language'>
        <Button aria-controls='simple-menu' aria-haspopup='true'>
          <img src='./language-globe.svg' className='language-globe' alt='select language' />
          {props.language}
        </Button>
      </div>

      <div className='logo'>
        <img src='./Owl.png' alt='Logo' className='logo-icon' />
        {props.language === 'DE' ? (
          <p>
            <b>Check dein Recht - kostenlos!</b>
            <br /> Finde heraus, in welchen Rechtsbereichen dir heute noch Geldansprüche zustehen können.
          </p>
        ) : (
          <p>
            <b>Check your claims for free!</b>
            <br /> Find out in which legal areas you could still be entitled to money claims today.
          </p>
        )}
      </div>

      <div className='profile'>
        <Button aria-controls='simple-menu2' aria-haspopup='true' onClick={handleClick}>
          <img src='./profile.svg' alt='profile' className='profile-icons' />
        </Button>
      </div>

      <Menu id='simple-menu' anchorEl={profileMenu} keepMounted open={Boolean(profileMenu)} onClose={handleClose}>
        <MenuItem onClick={handleClose}>Profile</MenuItem>
        <MenuItem onClick={handleClose}>
          <a href='https://app.gaius.legal' alt='gaius' target='_blank' rel='noopener noreferrer'>
            Login
          </a>
        </MenuItem>
      </Menu>
    </div>
  );
}
