import React, { Component } from 'react';
import classNames from 'classnames';
import './Message.css';

class Message extends Component {
  render() {
    const authorClass = this.props.author;
    let classAuthor = '';
    if (authorClass === '') {
      classAuthor = 'gaius-user'; //ClassName on Message send
    } else {
      classAuthor = 'gaius-bot';
    }
    const classes = classNames('Message ', classAuthor, {
      log: !this.props.author,
      me: this.props.me
    });

    return (
      <div className={classes}>
        {this.props.author && <span className='author'>{this.props.author}</span>}
        {this.props.body}
      </div>
    );
  }
}

export default Message;
