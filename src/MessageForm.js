import React, { Component } from 'react';
import { TextareaAutosize } from '@material-ui/core';
import './MessageForm.css';

class MessageForm extends Component {
  componentDidMount = () => {
    this.input.focus();
  };

  checkInputAndSubmit = input => {
    if (input.replace(/\s/g, '').length) {
      this.props.onMessageSend(input.trim());
    }
  };

  handleFormSubmit = event => {
    this.checkInputAndSubmit(this.input.value);
    this.input.value = '';
    var scr_top = document.getElementById('textFieldId');
    scr_top.scrollTop = 0;
    event.preventDefault();
  };

  handleChange = e => {
    let doc = document.getElementsByClassName('button-container');
    if (doc.length > 0) {
      doc[0].className = 'button-container-filled';
    }
    // let code = e.keyCode ? e.keyCode : e.which;
    // if (code === 13) {
    //   this.checkInputAndSubmit(e.target.value);
    //   e.target.value = '';
    //   doc = document.getElementsByClassName('button-container-filled');
    //   doc[0].className = 'button-container';
    // }
  };

  render() {
    return (
      <div>
        {this.props.typingIndicator ? (
          <div className='typingIndicator'>
            <div className='typingIndicatorContents'>
              <img className='bot-icon' src='gaiusbot.svg' alt='prof' />{' '}
              <div className='loader'>
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
          </div>
        ) : (
          <div></div>
        )}
        <form className='MessageForm' onSubmit={this.handleFormSubmit}>
          <div className='input-container'>
            <TextareaAutosize
              rowsMax={4}
              id='textFieldId'
              type='text'
              ref={node => (this.input = node)}
              placeholder='Enter your message...'
              disabled={this.props.chatDisable}
              onKeyPress={this.handleChange}
            />
          </div>
          <div className='button-container'>
            <button disabled={this.props.chatDisable} type='submit'>
              <svg _ngcontent-c20='' viewBox='0 0 512 512' xmlns='http://www.w3.org/2000/svg'>
                <path
                  _ngcontent-c20=''
                  d='M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z'
                ></path>
              </svg>
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default MessageForm;
