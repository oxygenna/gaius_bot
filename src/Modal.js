import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import './Modal.css';
const useStyles = makeStyles(theme =>
  createStyles({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  })
);

export default function TransitionsModal(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const handleClose = () => {
    setOpen(false);
    props.initChatBot();
  };

  return (
    <div>
      <Modal
        aria-labelledby='transition-modal-title'
        aria-describedby='transition-modal-description'
        className={classes.modal}
        open={open}
        // onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          <div className='Modal'>
            <div className='modal-title'>
              {props.language === 'DE' ? (
                <h2>Hallo und willkommen beim Gaius Fall-Detektor!</h2>
              ) : (
                <h2>Hello and welcome to the Gaius Claims Detector!</h2>
              )}
            </div>
            <div className='modal-img flex-center'>
              <img src='./Owl.png' alt='gaius owl' className='gaius-owl' />
            </div>
            {props.language === 'DE' ? (
              <div className='modal-text'>
                <p>
                  Verbraucher verschenken an vielen Stellen Geld, weil sie die ihnen zustehenden Rechte nicht kennen und
                  durchsetzen.
                </p>
                <p>
                  Hier findest du heraus, in welchen Bereichen dir heute noch Ansprüche zustehen und wie du sie
                  rechtlich geltend machst.
                </p>
                <p>Beantworte einfach ein paar Fragen und wir kümmern uns um den Rest.</p>
              </div>
            ) : (
              <div className='modal-text'>
                <p>
                  Did you know that in many places, consumers give away money not knowing and enforcing the rights to
                  which they are entitled.
                </p>
                <p>Here you can discover how much money you're owed and how to claim your rights.</p>
                <p>Just answer a few questions via chat and we will take care of the rest.</p>
              </div>
            )}
            <div className='modal-language'>
              {props.language === 'DE' ? (
                <p className='modal-text' style={{ display: 'inline-block' }}>
                  Wähle deine Sprache:
                </p>
              ) : (
                <p className='modal-text' style={{ display: 'inline-block' }}>
                  Choose language
                </p>
              )}

              <Button aria-controls='simple-menu' aria-haspopup='true' onClick={props.handleClickSelectionlanguage}>
                <img src='./language-globe.svg' className='language-globe' alt='select language' />
                {props.language}
              </Button>
              <Menu
                id='simple-menu'
                anchorEl={props.anchorEl}
                keepMounted
                open={Boolean(props.anchorEl)}
                onClose={() => props.onLanguageSelection(props.language)}
              >
                <MenuItem onClick={() => props.onLanguageSelection('DE')}>
                  <img src='./de.png' alt='language selection DE' className='language-selection' />
                  DE
                </MenuItem>

                <MenuItem onClick={() => props.onLanguageSelection('EN')}>
                  <img src='./en.png' alt='language selection DE' className='language-selection' />
                  EN
                </MenuItem>
              </Menu>
            </div>
            <div className='flex-center'>
              <Button className='buttonColor buttonModal' variant='contained' onClick={handleClose}>
                {props.language === 'DE' ? ' JETZT CHAT STARTEN' : 'Start Chat'}
              </Button>
            </div>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
